import axios from "axios";
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'
import Header from "./components/Header";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import Register from "./pages/Register";

export default function App(){

    return(
        <>
        <Router>
            <div className="container">
                <Header/>
                <Routes>
                <Route path="/login" element={<Login/>}/>
                <Route path="/Register" element={<Register/>}/>
                <Route path="/" element={<Dashboard/>}/>
                </Routes>
            </div>
        </Router>
        <ToastContainer/>
    </>
    );
}