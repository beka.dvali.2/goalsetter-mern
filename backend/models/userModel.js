const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'გთხოვთ შეავსოთ სახელის ველი']
    },
    email:{
        type:String,
        required:[true, 'გთხოვთ შეავსოთ მეილის ველი'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'გთხოვთ შეავსოთ პაროლის ველი']
    }
},
{
    timestamps:true
})

module.exports = mongoose.model('User',userSchema)